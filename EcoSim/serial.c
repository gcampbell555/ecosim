//
//  serial.c
//  EcoSim
//
//  Created by Greg Campbell on 8/11/14.
//  Copyright (c) 2014 Greg Campbell. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <string.h>
#include <errno.h>

#include "serial.h"


// --------------------------------------------------------------------------------------------------------------------
//  macros and defines
// --------------------------------------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------------------------------------
//  Static Function Prototypes
// --------------------------------------------------------------------------------------------------------------------



// --------------------------------------------------------------------------------------------------------------------
//  Global Variables
// --------------------------------------------------------------------------------------------------------------------



/******************************************************************************
 *
 * openPort()
 *
 * Function...
 *
 *
 * Assumes:
 *  -
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

int openPort(const char* deviceFile) {
    
    int             fd = -1;
    struct termios  portOptionsAsFound;
    struct termios  portOptions;
    
    
    // open file
    if((fd = open(deviceFile, O_RDWR | O_NOCTTY | O_NONBLOCK)) == -1 ) {
        
        printf("Error opening serial port %s - %s(%d).\n", deviceFile, strerror(errno), errno);
        goto ErrorExit;
    }
    
    // prevent other from opening file
    if(ioctl(fd, TIOCEXCL) == -1) {
        
        printf("Error setting TIOCEXCL on %s - %s(%d).\n", deviceFile, strerror(errno), errno);
        goto ErrorExit;
    }
    
#if 0
    // now that file is open, clear the no block flag
    if (fcntl(fd, F_SETFL, 0) ==  -1) {
        
        printf("Error clearing O_NONBLOCK %s - %s(%d).\n", deviceFile, strerror(errno), errno);
        goto ErrorExit;
    }
#endif
    
    // get current port options
    if (tcgetattr(fd, &portOptionsAsFound) == -1) {
        printf("Error getting tty attributes %s - %s(%d).\n", deviceFile, strerror(errno), errno);
        goto ErrorExit;
    }
    
    
    portOptions = portOptionsAsFound;
    
    cfmakeraw(&portOptions);
    cfsetspeed(&portOptions, B115200);      // Set 115200 baud
    portOptions.c_cc[VMIN]   = 1;
    portOptions.c_cc[VTIME]  = 10;
    portOptions.c_cflag     &= ~CSIZE;      // clear the size bits
    portOptions.c_cflag     |=  CS8;        // 8 bits
    portOptions.c_cflag     &= ~PARENB;     // no parity
    portOptions.c_iflag     &= ~INPCK;      // disable input parity checking
    portOptions.c_cflag     &= ~CSTOPB;     // 1 stop bit
    
    
    // update port options
    if (tcsetattr(fd, TCSANOW, &portOptions) == -1) {
        printf("Error setting tty attributes %s - %s(%d).\n", deviceFile, strerror(errno), errno);
        goto ErrorExit;
    }
    
    
    return fd;
    
    
ErrorExit:
    closePort(&fd);
    return fd;
}


/******************************************************************************
 *
 * closePort()
 *
 * Function...
 *
 *
 * Assumes:
 *  -
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

void closePort(int *fd) {
    
    if(*fd != -1) { close(*fd); }
    
    *fd = -1;
    
    return;
}


/******************************************************************************
 *
 * readPort()
 *
 * Function...
 *
 *
 * Assumes:
 *  -
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

int readPort(int fd, RingBuffer *buf) {
    
    char    byte;
    int     bytesRead = 0;
    
    while(read(fd, &byte, 1)) {
        writeBuffer(buf, (unsigned char)byte);
        bytesRead++;
    }
    
    return bytesRead;
}


/******************************************************************************
 *
 * initBuffer()
 *
 * Function...
 *
 *
 * Assumes:
 *  -
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

void initBuffer(RingBuffer *buf, int size) {
    buf->size = size + 1;
    buf->head = 0;
    buf->tail = 0;
    buf->elems = calloc(buf->size, sizeof(unsigned char));
    
    if(buf->elems == NULL) { buf->size = 0; }
    
    return;
}


/******************************************************************************
 *
 * initBuffer()
 *
 * Function...
 *
 *
 * Assumes:
 *  -
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

void freeBuffer(RingBuffer *buf) {
    free(buf->elems);
    
    return;
}


/******************************************************************************
 *
 * bufferFull()
 *
 * Function...
 *
 *
 * Assumes:
 *  -
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

int bufferFull(RingBuffer *buf) {
    return (((buf->tail + 1) % buf->size) == buf->head);
}


/******************************************************************************
 *
 * bufferEmpby()
 *
 * Function...
 *
 *
 * Assumes:
 *  -
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

int bufferEmpty(RingBuffer *buf) {
    return buf->head == buf->tail;
}


/******************************************************************************
 *
 * bufferDataSize()
 *
 * Function...
 *
 *
 * Assumes:
 *  -
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

int bufferDataSize(RingBuffer *buf) {
    
    return ((buf->tail + buf->size) - buf->head) % buf->size;
}


/******************************************************************************
 *
 * writeBuffer()
 *
 * Function...
 *
 *
 * Assumes:
 *  -
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

void writeBuffer(RingBuffer *buf, unsigned char byte) {
    
    // write byte to buffer
    buf->elems[buf->tail] = byte;
    
    // update tail index
    buf->tail = (buf->tail + 1) % buf->size;
    
    // check for overflow
    if(buf->head == buf->tail) {
        buf->head = (buf->head + 1) % buf->size;
    }
}



/******************************************************************************
 *
 * readBuffer()
 *
 * Function...
 *
 *
 * Assumes:
 *  - buffer is not empty; can check with bufferEmpty().
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

unsigned char readBuffer(RingBuffer *buf) {
    
    // check if buffer empty
    if(bufferEmpty(buf)) { return 0; }
    
    // read byte
    unsigned char byte = buf->elems[buf->head];
    
    // update head pointer
    buf->head = (buf->head + 1) % buf->size;
    
    return byte;
}


/******************************************************************************
 *
 * unreadBuffer()
 *
 * Function...
 *
 *
 * Assumes:
 *  - buffer is not empty; can check with bufferEmpty().
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

void unreadBuffer(RingBuffer *buf, unsigned int bytes) {
    
    buf->head = ((buf->head + buf->size) - bytes) % buf->size;
}


/******************************************************************************
 *
 * readBuffer()
 *
 * Function...
 *
 *
 * Assumes:
 *  - buffer is not empty; can check with bufferEmpty().
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

void displayBuffer(RingBuffer *buf) {
    
    int     i;
    int     j;
    
    printf("size:      %i\n", buf->size);
    printf("head:      %i\n", buf->head);
    printf("tail:      %i\n", buf->tail);
    printf("data size: %i\n", bufferDataSize(buf));
    printf("data:      [00]");
    //  for(i=0, j=buf->head; i<bufferDataSize(buf); i++, j++) { printf(" %02X", buf->elems[(j % buf->size)]); }
    //  printf("\n");
    
    
    
    for(i=0, j=buf->head; i<bufferDataSize(buf); i++, j++) {
        
        if(((i % 16) == 0) && (i != 0)) {
            printf("\n           [%02i]", (i / 16));
        }
        else if(((i %  8) == 0) && (i != 0)) {
            printf(" -");
        }
        
        printf(" %02X", buf->elems[(j % buf->size)]);
    }
    printf("\n");
    
    
    
    printf("\n");
}


//=====================================================================================================================
//
// function()
//
// Function...
//
//
// Assumes:
//  -
//
//
// Parameters:
//  -
//
//
// Returns:
//  -
//
//
// Notes:
//
//
//=====================================================================================================================

/* Subtract the ‘struct timeval’ values X and Y, storing the result in RESULT.
 Return 1 if the difference is negative, otherwise 0. */
/*
 int timevalDiff(struct timeval t1, struct timeval t2, struct timeval *result) {
 
 // Perform the carry for the later subtraction by updating y.
 
 if (t1->tv_usec < t2->tv_usec) {
 
 int nsec = (t2->tv_usec - t1->tv_usec) / 1000000 + 1;
 
 t2->tv_usec -= 1000000 * nsec;
 t2->tv_sec  += nsec;
 }
 
 if (t1->tv_usec - t2->tv_usec > 1000000) {
 
 int nsec = (t1->tv_usec - t2->tv_usec) / 1000000;
 
 t2t2->tv_usec += 1000000 * nsec;
 t2->tv_sec    -= nsec;
 }
 
 
 // Compute the time remaining to wait. tv_usec is certainly positive.
 
 result->tv_sec = t1->tv_sec - t2->tv_sec;
 result->tv_usec = t1->tv_usec - t2->tv_usec;
 
 
 // Return 1 if result is negative.
 
 return t1->tv_sec < t2->tv_sec;
 }
 */

