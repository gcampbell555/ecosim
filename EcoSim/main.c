//
//  main.c
//  EcoSim
//
//  Created by Greg Campbell on 8/5/14.
//  Copyright (c) 2014 Greg Campbell. All rights reserved.
//

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/select.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>

#include "ecolink.h"
#include "serial.h"

// --------------------------------------------------------------------------------------------------------------------
//  defines
// --------------------------------------------------------------------------------------------------------------------

//#define SENSOR_DEVICE_FILE      "/dev/cu.usbserial"
//#define SENSOR_DEVICE_FILE      "/dev/cu.usbserial-A700eHWV"
//#define SENSOR_DEVICE_FILE      "/dev/cu.usbserial-A700eHM2"

#define ECOLINK_CONFIG_FILE     "/Users/gregcampbell/iControl/Workspace/EcoSim/EcoSim/ecoSimConfig.xml"

#define SENSOR_DEVICE_FILE      "/dev/tty.usbserial-AH02MIES"

#define SENSOR_DATA_FILE        "/Users/gregcampbell/iControl/Workspace/EcoSim/EcoSim/sensorData.txt"


// --------------------------------------------------------------------------------------------------------------------
//  data structures
// --------------------------------------------------------------------------------------------------------------------

typedef struct {
    char   *devFile;
    int     ackDelay;
    int     hbDelay;
} ecoSimConfig;


// --------------------------------------------------------------------------------------------------------------------
//  Function Prototypes
// --------------------------------------------------------------------------------------------------------------------

char *printTime(struct timeval *time, char *timeString);
int delayExpired(struct timeval start, struct timeval delay);


// --------------------------------------------------------------------------------------------------------------------
//  Static Function Prototypes
// --------------------------------------------------------------------------------------------------------------------



// --------------------------------------------------------------------------------------------------------------------
//  Global Variables
// --------------------------------------------------------------------------------------------------------------------

int sensorFd;



/**********************************************************************************************************************
 *
 *  main()
 *
 *  Function...
 *
 *  Input:
 *  - Nothing
 *
 *  Output:
 *  - Nothing
 *
 *  Assumes:
 *  - Nothing
 *
 *  Notes:
 *
 **********************************************************************************************************************/

int main(int argc, char **argv) {
    
    // sensor data file
    FILE               *sensorFile = NULL;
    ecoLinkSensor       sensor;
    
    int                 lineCount   = 0;
    char               *lineBuf     = NULL;
    size_t              lineBufLen  = 1024;
    
    ecoLinkStatusPkt    pkt;
    unsigned char      *rawPkt;
    char                string[128];
    
    struct timeval      startTime;
    struct timeval      delayTime;
    struct timeval      currTime;
    
    char                timeString[20];
    
    int                 pendingPkt = 0;
    
    
    // ecolink serial
    int                 serialFd    = -1;
    
    char                byte;
    RingBuffer          readBuf;
    
    EcolinkParserState  pktState;
    
    int                 result;
    int                 nFds;
    fd_set              readFds;
    struct timeval      selectTimeout = { 0l, 1000 };
    
    
    //  other...
    int                 done = 0;
    
    
    // -----------------------------------------------------------------------------------------------------------------
    //  initialize ecolink parser
    // -----------------------------------------------------------------------------------------------------------------
    
    
    
    
    
    getConfig(ECOLINK_CONFIG_FILE);

    return 0;
    
    
    lineBuf = malloc(lineBufLen);
    initBuffer(&readBuf, 256);
    resetEcolinkParser(&pktState);
    
    
    // open sensor data file
    if((sensorFile = fopen(SENSOR_DATA_FILE, "r")) == NULL) {
        printf("Unable to open file %s - %s(%d).\n", SENSOR_DATA_FILE, strerror(errno), errno);
        return -1;
    }

    

    getConfigData( sensorFile );
    
    return 0;


    
    // open serial port
    if((serialFd = openPort(SENSOR_DEVICE_FILE)) == -1) {
        // unable to open serial
        return -2;
    }
    
    printf("serialFd: [%i] %s\n", serialFd, SENSOR_DEVICE_FILE);
    
    
    // get starting time
    gettimeofday(&startTime, NULL);
    printf("[%s] start\n", printTime(&startTime, timeString));
    
    
    while(!done) {
        
        if(!pendingPkt) {
            
            if(getline(&lineBuf, &lineBufLen, sensorFile) == -1) {
                
                // no more packet data, exit...
                done = 1;
            }
            else {
                
                //  make ecolink status packet from test data record
                
                // check for comments or empty lines in data file
                if((lineBuf[0] == '#') || (lineBuf[0] == 0x0A)) { continue; }
                
                // process the debug data and put result in sensor data structure
                delayTime = processDebugData(lineBuf, &sensor);
                
                // build packet
                if(makeEcolinkStatusPkt(&pkt, &sensor) != 0) { continue; }
                
                pendingPkt = 1;
            }
        }
        else {
            
            // packet waiting to be sent
            if(delayExpired(startTime, delayTime)) {
                
                // print packet (could/should be log message)
                gettimeofday(&currTime, NULL);
                printf("[%s] %s\n", printTime(&currTime, timeString), printEcolinkPacket(pkt, string));
                
                rawPkt = (unsigned char *)&pkt;
                write(serialFd, (unsigned char *)&pkt, (pkt.len + 2));
                
                delayTime.tv_usec += 100000;
                
                delayTime.tv_sec  += (delayTime.tv_usec / 1000000);
                delayTime.tv_usec += (delayTime.tv_usec % 1000000);
                
                //pendingPkt = 0;
            }
        }
        
        
        FD_ZERO(&readFds);
        FD_SET(serialFd, &readFds);
        nFds = serialFd + 1;
        
        result = select(nFds, &readFds, NULL, NULL, &selectTimeout);
        
        if(result == 0) {
            
            // select timeout
        }
        else if(result == -1) {
            
            // select error
        }
        else {
            
            // read available data from serial port
            if(read(serialFd, &byte, 1) > 0) {
                writeBuffer(&readBuf, (unsigned char)byte);
            }
            
            //          printf("data received...\n");
            //          displayBuffer(&readBuf);
            
            // check for ecolink packet
            if(ecolinkParser(&pktState, &readBuf) == PACKET_AVAILABLE) {
                
                unsigned char       pktBuf[32];
                unsigned char       pktBufLen = sizeof(pktBuf);
                ecoLinkStatusPkt   *statusPkt = (ecoLinkStatusPkt *)pktBuf;
                char                statusString[256];
                
                getEcolinkPacket(&pktState, pktBuf, &pktBufLen);
                
                gettimeofday(&currTime, NULL);
                printf("[%li.%03i] rcvPkt: %s\n", currTime.tv_sec, (currTime.tv_usec / 1000), printEcolinkPacket(*statusPkt, statusString));
                
                if(pendingPkt && (pktBuf[1] == 1)) {
                    pendingPkt = 0;
                }
            }
        }
        
        
    }
    
    
    
#if 0
    while(getline(&lineBuf, &lineBufLen, sensorFile) != -1) {
        
        // ------------------------------------------------------------------------------------------------------------
        //  make ecolink status packet from test data record
        // ------------------------------------------------------------------------------------------------------------
        
        // check for comments or empty lines in data file
        if((lineBuf[0] == '#') || (lineBuf[0] == 0x0A)) { continue; }
        
        // process the debug data and put result in sensor data structure
        delayTime = processDebugData(lineBuf, &sensor);
        
        // build packet
        if(makeEcolinkStatusPkt(&pkt, &sensor) != 0) { continue; }
        
        // wait to send packet
        while(!delayExpired(startTime, delayTime));
        
        // print packet (could/should be log message)
        gettimeofday(&currTime, NULL);
        printf("[%s] %s\n", printTime(&currTime, timeString), printEcolinkPacket(pkt, string));
        
        rawPkt = (unsigned char *)&pkt;
        write(serialFd, (unsigned char *)&pkt, (pkt.len + 2));
        
    }
#endif
    
    gettimeofday(&currTime, NULL);
    printf("[%s] stop\n", printTime(&currTime, timeString));
    
    
    free(lineBuf);
    fclose(sensorFile);
    
    
    closePort(&serialFd);
    
    
    return 0;
}


// ====================================================================================================================
//
//  printTime()
//
//  If a time is passed to the function that time is returned as a string of the decimal time. If NULL is passed,
//  then the current time is reported.
//
//  Input:
//  - Nothing
//
//  Output:
//  - Nothing
//
//  Assumes:
//  - Nothing
//
//  Notes:
//
// ====================================================================================================================

char *printTime(struct timeval *time, char *timeString) {
    
    struct timeval *timeValue = time;
    struct timeval  currTime;
    
    if(timeValue == NULL) {
        timeValue = &currTime;
        gettimeofday(timeValue, NULL);
    }
    
    
    sprintf(timeString, "%li.%03i", timeValue->tv_sec, (timeValue->tv_usec / 1000));
    
    
    return timeString;
}


// ====================================================================================================================
//
//  delayExpired()
//
//  If a time is passed to the function that time is returned as a string of the decimal time. If NULL is passed, 
//  then the current time is reported.
//
//  Input:
//  - Nothing
//
//  Output:
//  - Nothing
//
//  Assumes:
//  - Nothing
//
//  Notes:
//
// ====================================================================================================================

int delayExpired(struct timeval start, struct timeval delay) {
    
    struct timeval  currTime;
    
    
    gettimeofday(&currTime, NULL);
    
    if(((start.tv_sec  + delay.tv_sec ) <= currTime.tv_sec ) && 
       ((start.tv_usec + delay.tv_usec) <= currTime.tv_usec)    ) { 
        
        return 1; 
    }
    
    return 0;
}


// ====================================================================================================================
//
//  delayExpired()
//
//  If a time is passed to the function that time is returned as a string of the decimal time. If NULL is passed,
//  then the current time is reported.
//
//  Input:
//  - Nothing
//
//  Output:
//  - Nothing
//
//  Assumes:
//  - Nothing
//
//  Notes:
//
// ====================================================================================================================

void getConfig(const char *file) {
    
    ezxml_t     cfg = ezxml_parse_file(file);
    ezxml_t     child;
    
    
    if((child = ezxml_child(cfg, "serialPort")) != NULL) {
        
        printf("devFile:  %s\n", ezxml_attr(child, "devFile"));
    }
    
    
    if((child = ezxml_child(cfg, "ackDelay")) != NULL) {
        
        printf("ackDelay: %s\n", ezxml_attr(child, "value"));
    }
    
    
    if((child = ezxml_child(cfg, "hbDelay")) != NULL) {
        
        printf("hbDelay:  %s\n", ezxml_attr(child, "value"));
    }
    
    
    printf("\n");
    
    
    
    return;
}

