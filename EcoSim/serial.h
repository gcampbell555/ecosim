//
//  serial.h
//  EcoSim
//
//  Created by Greg Campbell on 8/11/14.
//  Copyright (c) 2014 Greg Campbell. All rights reserved.
//


#ifndef _SERIAL_H_
#define _SERIAL_H_


// --------------------------------------------------------------------------------------------------------------------
//  macro and defines
// --------------------------------------------------------------------------------------------------------------------



// --------------------------------------------------------------------------------------------------------------------
//  data structures
// --------------------------------------------------------------------------------------------------------------------

typedef struct {
    int             size;
    int             head;
    int             tail;
    unsigned char  *elems;
} RingBuffer;


// --------------------------------------------------------------------------------------------------------------------
//  Function Prototypes
// --------------------------------------------------------------------------------------------------------------------

int  openPort(const char* deviceFile);
void closePort(int *fd);

int writePort(int fd, unsigned char *buf, int bufLen);
int readPort(int fd, RingBuffer *buf);

void initBuffer(RingBuffer *buf, int size);
void freeBuffer(RingBuffer *buf);
int bufferFull(RingBuffer *buf);
int bufferEmpty(RingBuffer *buf);
int bufferDataSize(RingBuffer *buf);
void writeBuffer(RingBuffer *buf, unsigned char byte);
unsigned char readBuffer(RingBuffer *buf);
void unreadBuffer(RingBuffer *buf, unsigned int bytes);

void displayBuffer(RingBuffer *buf);


// --------------------------------------------------------------------------------------------------------------------
//  Global Variables
// --------------------------------------------------------------------------------------------------------------------


#endif /* _SERIAL_H_ */
