//
//  ecolink.h
//  EcoSim
//
//  Created by Greg Campbell on 8/11/14.
//  Copyright (c) 2014 Greg Campbell. All rights reserved.
//

#ifndef _ECOLINK_H_
#define _ECOLINK_H_

#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "ezxml.h"

#include "serial.h"


// --------------------------------------------------------------------------------------------------------------------
//  macro and defines
// --------------------------------------------------------------------------------------------------------------------

// parser state
#define READY                               (0)
#define START_OF_PACKET                     (1)
#define READ_DATA                           (2)
#define PACKET_AVAILABLE                    (3)


// ecolink packet types
#define ECOLINK_NO_PKT                      (0)
#define ECOLINK_ACK_PKT                     (1)
#define ECOLINK_STATUS_PKT                  (2)


// packet definitions
#define ECOLINK_PKT_START_OFFSET            (0)
#define ECOLINK_PKT_LENGTH_OFFSET           (1)
#define ECOLINK_PKT_TYPE_OFFSET             (2)

#define ECOLINK_START_BYTE                  (0x55)


//
// ecolink status packet definitions
//

// packet type
#define ECOLINK_TYPE_HONEYWELL              ('H')
#define ECOLINK_TYPE_2GIG                   ('2')
#define ECOLINK_TYPE_ECOLINK                ('E')


// status byte bit definitions for device type 1
#define ECOLINK_DEV_TYPE_1                  (1)
#define ECOLINK_DEV_TYPE_1_REED_SW          (0x01)
#define ECOLINK_DEV_TYPE_1_AUX_1            (0x02)
#define ECOLINK_DEV_TYPE_1_BIT2             (0x04)
#define ECOLINK_DEV_TYPE_1_BIT3             (0x08)
#define ECOLINK_DEV_TYPE_1_TAMPER_SW        (0x10)
#define ECOLINK_DEV_TYPE_1_LOW_BATTERY      (0x20)
#define ECOLINK_DEV_TYPE_1_LEARN_ME_REQ     (0x40)
#define ECOLINK_DEV_TYPE_1_SUPERVISORY      (0x80)
#define ECOLINK_DEV_TYPE_1_LENGTH           (7)


// status byte bit definitions for device type 8
#define ECOLINK_DEV_TYPE_8                  (8)
#define ECOLINK_DEV_TYPE_8_BIT0             (0x01)
#define ECOLINK_DEV_TYPE_8_POWER_ON_RESET   (0x02)
#define ECOLINK_DEV_TYPE_8_SUPERVISORY      (0x04)
#define ECOLINK_DEV_TYPE_8_LOW_BATTERY      (0x08)
#define ECOLINK_DEV_TYPE_8_AUX_2            (0x10)
#define ECOLINK_DEV_TYPE_8_AUX_1            (0x20)
#define ECOLINK_DEV_TYPE_8_TAMPER_SW        (0x40)
#define ECOLINK_DEV_TYPE_8_REED_SW          (0x80)
#define ECOLINK_DEV_TYPE_8_LENGTH           (7)


// max packet length value
#define ECOLINK_MAX_PACKET_SIZE             (7)


// parser constants
#define ECOLINK_PARSER_TIMEOUT_MS           (5)
#define ECOLINK_PARSER_BUFFER_SIZE          (256)


// error codes
#define ECOLINK_OK                          ( 0)
#define ECOLINK_ERR_GENERAL                 (-1)
#define ECOLINK_ERR_BUFFER_TOO_SMALL        (-2)
#define ECOLINK_ERR_UNKOWN_DEVICE_TYPE      (-3)


// --------------------------------------------------------------------------------------------------------------------
//  data structures
// --------------------------------------------------------------------------------------------------------------------

// ack packet
typedef struct ecoLinkAckPkt {
    unsigned char   start;
    unsigned char   len;
    unsigned char   checksum;
} EcoLinkAckPkt;


// sensor status data
typedef struct ecoLinkSensor {
    unsigned char   packetType;
    unsigned char   deviceType;
    int             serialNumber;
    int             rssi;
    int             reedSwitch;
    int             tamperSwitch;
    int             aux1;
    int             aux2;
    int             lowBattery;
    int             superviosry;
    int             por;
    int             learnMeReq;
} ecoLinkSensor;


// sensor status packet
typedef struct ecoLinkSensorStatusPkt {
    unsigned char   start;
    unsigned char   len;
    unsigned char   type;
    unsigned char   sn[3];
    unsigned char   status;
    unsigned char   rssi;
    unsigned char   checksum;
} ecoLinkStatusPkt;


// parser state
typedef struct ecolinkParserState {
    int             state;
    struct timeval  timeout;
    struct timeval  timeoutValue;
    int             bufLen;
    unsigned char   buf[ECOLINK_PARSER_BUFFER_SIZE];
} EcolinkParserState;


// --------------------------------------------------------------------------------------------------------------------
//  prototypes
// --------------------------------------------------------------------------------------------------------------------

void getConfig(const char *file);

void getConfigData( FILE *sensorFile );
struct timeval processDebugData(char *buf, ecoLinkSensor *sensor);

int makeEcolinkStatusPkt(ecoLinkStatusPkt *buf, ecoLinkSensor *sensor);
int procEcolinkStatusPkt(ecoLinkStatusPkt *buf, ecoLinkSensor *sensor);
char* printEcolinkPacket(ecoLinkStatusPkt pkt, char *string);

unsigned char calcChecksum(ecoLinkStatusPkt *pkt);
void addChecksum (ecoLinkStatusPkt *pkt);
int validChecksum(ecoLinkStatusPkt *pkt);

void resetEcolinkParser(EcolinkParserState *state);
int ecolinkParser(EcolinkParserState *state, RingBuffer *buf);
int getEcolinkPacket(EcolinkParserState *state, unsigned char *pkt, unsigned char *pktLen);
int ecolinkTimeout(EcolinkParserState *state);
void setEcolinkTimeout(long ms);
void printEcolinkParserState(EcolinkParserState *state);


// --------------------------------------------------------------------------------------------------------------------
//  global variables
// --------------------------------------------------------------------------------------------------------------------



#endif /* _ECOLINK_H_ */
