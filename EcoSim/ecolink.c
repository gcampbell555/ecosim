//
//  ecolink.c
//  EcoSim
//
//  Created by Greg Campbell on 8/11/14.
//  Copyright (c) 2014 Greg Campbell. All rights reserved.
//

#include <stdio.h>
#include <sys/time.h>

#include "ecolink.h"
#include "serial.h"


// --------------------------------------------------------------------------------------------------------------------
//  macros and defines
// --------------------------------------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------------------------------------
//  Static Function Prototypes
// --------------------------------------------------------------------------------------------------------------------



// --------------------------------------------------------------------------------------------------------------------
//  Global Variables
// --------------------------------------------------------------------------------------------------------------------



void getConfigData( FILE *sensorFile ) {

    int                 lineCount   = 0;
    char               *lineBuf     = NULL;
    size_t              lineBufLen  = 1024;

    char                serialDevFile[1024] = { 0 };

    char               *token;

    
    lineBuf = malloc(lineBufLen);

    while(getline(&lineBuf, &lineBufLen, sensorFile) != -1) {
        
        printf("[%02i] %s", lineCount++, lineBuf);

        if( strstr(lineBuf, "StartOfSensorData") != NULL) { break; }

        token = strtok(lineBuf, " \t\n");       
        if( token == NULL ) { continue; }

        if( strcmp(token, "SerialPort") == 0 ) { strcpy(serialDevFile, strtok(NULL, " \t\n")); }

    }

    printf("serialDevFile: %s\n", serialDevFile);

    free(lineBuf);
}

/******************************************************************************
 *
 *  procDebugData()
 *
 *  Function processes a line of data from ecolink simulator data file and
 *  places the result into an ecolink sensor data structure.
 *
 *
 * Assumes:
 *  - The format of the data in the buffer conforms to the expected structure
 *    for ecolink simulator data, with the data fields seperated by a space
 *    character (0x20) and terminated by a linefeed character (0x0a).
 *
 * Parameters:
 *  - buf:     pointer to char buffer containing the sensor data
 *  - sensor:  pointer to ecolink sensor data structure for processed sensor
 *             data
 *
 * Returns:
 *  - txDelay: delay in seconds for trasmission of sumlated sensor data in
 *             seconds.
 *
 * Notes:
 *
 *
 ******************************************************************************/

struct timeval processDebugData(char *buf, ecoLinkSensor *sensor) {
    
    struct timeval      delay = { 0l, 0 };
    char               *token;
    char               *type;
    
    
    // initialize sensor to
    sensor->packetType   =  0;
    sensor->deviceType   =  0;
    sensor->serialNumber = -1;
    sensor->rssi         = -1;
    sensor->reedSwitch   = -1;
    sensor->tamperSwitch = -1;
    sensor->aux1         = -1;
    sensor->aux2         = -1;
    sensor->lowBattery   = -1;
    sensor->superviosry  = -1;
    sensor->por          = -1;
    sensor->learnMeReq   = -1;
    
    
    // get tx delay time
    delay.tv_sec         = strtoul(strtok (buf,  ".\n"), NULL, 10);
    delay.tv_usec        = (int)strtoul(strtok (NULL, " \n"), NULL, 10) * 1000;
    
    
    // parse debug packet data
    type                  = strtok (NULL, " \n");
    sensor->packetType    = *type;
    
    
    sensor->serialNumber  = (int)strtol( strtok (NULL,    " \n"), NULL,  0);
    sensor->deviceType    = ((sensor->serialNumber & 0xF00000) >> 20);
    sensor->serialNumber &= 0x0FFFFF;
    
    sensor->rssi          = (int)strtol( strtok (NULL,    " \n"), NULL, 10);
    
    while((token = strtok (NULL,    " \n")) != NULL) {
        
        if     (!strcmp(token, "REED_SW_ON"      )) { sensor->reedSwitch   = 1; }
        else if(!strcmp(token, "REED_SW_OFF"     )) { sensor->reedSwitch   = 0; }
        else if(!strcmp(token, "TAMPER_SW_ON"    )) { sensor->tamperSwitch = 1; }
        else if(!strcmp(token, "TAMPER_SW_OFF"   )) { sensor->tamperSwitch = 0; }
        else if(!strcmp(token, "AUX1_ON"         )) { sensor->aux1         = 1; }
        else if(!strcmp(token, "AUX1_OFF"        )) { sensor->aux1         = 0; }
        else if(!strcmp(token, "AUX2_ON"         )) { sensor->aux2         = 1; }
        else if(!strcmp(token, "AUX2_OFF"        )) { sensor->aux2         = 0; }
        else if(!strcmp(token, "LOW_BATT_ON"     )) { sensor->lowBattery   = 1; }
        else if(!strcmp(token, "LOW_BATT_OFF"    )) { sensor->lowBattery   = 0; }
        else if(!strcmp(token, "SUPERVISORY_ON"  )) { sensor->superviosry  = 1; }
        else if(!strcmp(token, "SUPERVISORY_OFF" )) { sensor->superviosry  = 0; }
        else if(!strcmp(token, "POR_ON"          )) { sensor->por          = 1; }
        else if(!strcmp(token, "POR_OFF"         )) { sensor->por          = 0; }
        else if(!strcmp(token, "LEARN_ME_REQ_ON" )) { sensor->learnMeReq   = 1; }
        else if(!strcmp(token, "LEARN_ME_REQ_OFF")) { sensor->learnMeReq   = 0; }
    }
    
    
    return delay;
}


/******************************************************************************
 *
 * makeEcolinkStatusPkt()
 *
 * Make an ecolink sensor status packet from the sensor data structure. This
 * function is intended to make packets for an ecolink sensor simulator and
 * is not expected to be used otherwise.
 *
 *
 * Assumes:
 *  - buffer parameter sufficient
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

int makeEcolinkStatusPkt(ecoLinkStatusPkt *pkt, ecoLinkSensor *sensor) {
    
    unsigned char  *raw = (unsigned char *)pkt;
    
    int             index  = 0;
    int             length = 0;
    unsigned char   status = 0;
    
    
    // build status byte per device type
    switch(sensor->deviceType) {
            
        case ECOLINK_DEV_TYPE_1:
            
            length = ECOLINK_DEV_TYPE_1_LENGTH;
            
            if(sensor->reedSwitch   == 1) { status |= ECOLINK_DEV_TYPE_1_REED_SW;        }
            if(sensor->aux1         == 1) { status |= ECOLINK_DEV_TYPE_1_AUX_1;          }
            if(sensor->tamperSwitch == 1) { status |= ECOLINK_DEV_TYPE_1_TAMPER_SW;      }
            if(sensor->lowBattery   == 1) { status |= ECOLINK_DEV_TYPE_1_LOW_BATTERY;    }
            if(sensor->learnMeReq   == 1) { status |= ECOLINK_DEV_TYPE_1_LEARN_ME_REQ;   }
            if(sensor->superviosry  == 1) { status |= ECOLINK_DEV_TYPE_1_SUPERVISORY;    }
            break;
            
            
        case ECOLINK_DEV_TYPE_8:
            
            length = ECOLINK_DEV_TYPE_8_LENGTH;
            
            if(sensor->por          == 1) { status |= ECOLINK_DEV_TYPE_8_POWER_ON_RESET; }
            if(sensor->superviosry  == 1) { status |= ECOLINK_DEV_TYPE_8_SUPERVISORY;    }
            if(sensor->lowBattery   == 1) { status |= ECOLINK_DEV_TYPE_8_LOW_BATTERY;    }
            if(sensor->aux2         == 1) { status |= ECOLINK_DEV_TYPE_8_AUX_2;          }
            if(sensor->aux1         == 1) { status |= ECOLINK_DEV_TYPE_8_AUX_1;          }
            if(sensor->tamperSwitch == 1) { status |= ECOLINK_DEV_TYPE_8_TAMPER_SW;      }
            if(sensor->reedSwitch   == 1) { status |= ECOLINK_DEV_TYPE_8_REED_SW;        }
            break;
            
            
        default:
            return ECOLINK_ERR_GENERAL;
    }
    
    
    // build packet
    raw[index++] = ECOLINK_START_BYTE;
    raw[index++] = length;
    raw[index++] = sensor->packetType;
    raw[index++] = ((sensor->serialNumber >> 16) & 0x0F) | ((sensor->deviceType << 4) & 0xF0);
    raw[index++] = ((sensor->serialNumber >>  8) & 0xFF);
    raw[index++] = ( sensor->serialNumber        & 0xFF);
    raw[index++] = status;
    raw[index++] = sensor->rssi;
    
    
    // add checksum
    addChecksum(pkt);
    
    
    return ECOLINK_OK;
}


/******************************************************************************
 *
 * openPort()
 *
 * Function...
 *
 *
 * Assumes:
 *  - buffer parameter sufficient
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

unsigned char calcChecksum(ecoLinkStatusPkt *pkt) {
    
    int             index    = 1;   // points to first byte after packet length
    unsigned char   checksum = 0;
    unsigned char  *raw      = (unsigned char *)pkt;
    
    
    for(int i=0; i<pkt->len; i++) { checksum += raw[index++]; }
    checksum = 0xFF - checksum;
    
    
    return checksum;
}


/******************************************************************************
 *
 * openPort()
 *
 * Function...
 *
 *
 * Assumes:
 *  - buffer parameter sufficient
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

void addChecksum (ecoLinkStatusPkt *pkt) {
    
    unsigned char  *raw = (unsigned char *)pkt;
    
    
    raw[pkt->len + 1] = calcChecksum(pkt);
    
    
    return;
}


/******************************************************************************
 *
 * openPort()
 *
 * Function...
 *
 *
 * Assumes:
 *  - buffer parameter sufficient
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

int validChecksum(ecoLinkStatusPkt *pkt) {
    
    int             index    = 1;  // points to byte after packet length
    unsigned char   checksum = 0;
    unsigned char  *raw      = (unsigned char *)pkt;
    
    
    for(int i=0; i<(pkt->len + 1); i++) { checksum += raw[index++]; }
    
    
    if(checksum == 0xFF) { return 1; }
    else                 { return 0; }
    
}


/******************************************************************************
 *
 * openPort()
 *
 * Function...
 *
 *
 * Assumes:
 *  - buffer parameter sufficient
 *
 *
 * Parameters:
 *  -
 *
 *
 * Returns:
 *  -
 *
 *
 * Notes:
 *
 *
 ******************************************************************************/

char* printEcolinkPacket(ecoLinkStatusPkt pkt, char *string) {
    
    unsigned char  *raw = (unsigned char *)&pkt;
    
    
    memset(string, 0, 20);
    
    
    sprintf(string, "%02X%02X", raw[0], raw[1]);
    for(int i=0; i<raw[1]; i++) { sprintf(string, "%s%02X", string, raw[i+2]); }
    
    
    return string;
}




int packetAvailable(RingBuffer *buf, ecoLinkStatusPkt *pkt) {
    return 0;
}


//=====================================================================================================================
//
// resetEcolinkParser()
//
// Resets the parser state
//
//
// Assumes:
//  - buffer parameter sufficient
//
//
// Parameters:
//  -
//
//
// Returns:
//  -
//
//
// Notes:
//
//
//=====================================================================================================================

void resetEcolinkParser(EcolinkParserState *state) {
    
    state->state                = READY;
    state->timeout.tv_sec       =  0l;
    state->timeout.tv_usec      =  0;
    state->timeoutValue.tv_sec  =  ECOLINK_PARSER_TIMEOUT_MS / 1000;
    state->timeoutValue.tv_usec = (ECOLINK_PARSER_TIMEOUT_MS % 1000) * 1000;
    state->bufLen               =  0;
    
    return;
}


//=====================================================================================================================
//
// function()
//
// Function...
//
//
// Assumes:
//  -
//
//
// Parameters:
//  -
//
//
// Returns:
//  -
//
//
// Notes:
//
//
//=====================================================================================================================

int ecolinkParser(EcolinkParserState *state, RingBuffer *buf) {
    
    unsigned char   byte;
//  struct timeval  currTime;
    
    
    switch(state->state) {
            
        case READY:
            
            while (!bufferEmpty(buf)) {
                
                if((byte = readBuffer(buf)) == ECOLINK_START_BYTE) {
                    
                    gettimeofday(&state->timeout, NULL);
                    state->timeout.tv_sec  += state->timeoutValue.tv_sec;
                    state->timeout.tv_usec += state->timeoutValue.tv_usec;
                    state->timeout.tv_sec  += (state->timeout.tv_usec / 1000000);
                    state->timeout.tv_usec  = (state->timeout.tv_usec % 1000000);
                    
                    state->buf[state->bufLen++] = byte;
                    state->state                = START_OF_PACKET;
                    break;
                }
            }
            break;
            
            
        case START_OF_PACKET:
            
            if(!bufferEmpty(buf)) {
                
                if((byte = readBuffer(buf)) <= ECOLINK_MAX_PACKET_SIZE) {
                    
                    // valid packet length byte, add to buffer and go to DATA_READ state
                    state->buf[state->bufLen++] = byte;
                    state->state                = READ_DATA;
                }
                else {
                    
                    // invalid packet length byte, return byte to buffer and return to READY state
                    unreadBuffer(buf, 1);
                    resetEcolinkParser(state);
                }
            }
            else if(ecolinkTimeout(state)) {
                
                // timeout waiting for packet length, return to READY state
                resetEcolinkParser(state);
            }
            break;
            
            
        case READ_DATA:
            
            if(bufferDataSize(buf) >= state->buf[ECOLINK_PKT_LENGTH_OFFSET]) {
                
                // read remaining packet data
                for(int i=0; i<state->buf[ECOLINK_PKT_LENGTH_OFFSET]; i++) { state->buf[state->bufLen++] = readBuffer(buf); }
                
                if(validChecksum((ecoLinkStatusPkt *)state->buf)) {
                    
                    // valid checksum, go to PACKET_AVAILABLE state
                    state->state = PACKET_AVAILABLE;
                }
                else {
                    
                    // invalid checksum, discard start byte and return to ready state
                    unreadBuffer(buf, (state->bufLen - 1));
                    resetEcolinkParser(state);
                }
                
            }
            else if(ecolinkTimeout(state)) {
                
                // timed out waiting for packet data, discard start byte and return to READY state
                unreadBuffer(buf, 1);
                resetEcolinkParser(state);
            }
            break;
            
            
        case PACKET_AVAILABLE:
            break;
            
            
        default:
            break;
    }
    
    return state->state;
}


//=====================================================================================================================
//
// function()
//
// Function...
//
//
// Assumes:
//  -
//
//
// Parameters:
//  -
//
//
// Returns:
//  -
//
//
// Notes:
//
//
//=====================================================================================================================

int ecolinkTimeout(EcolinkParserState *state) {
    
    struct timeval  currTime;
    
    gettimeofday(&currTime, NULL);
    
    if(currTime.tv_sec > state->timeout.tv_sec) {
        printf("[1] timeout; currTime = %li.%06i - timeout = %li.%06i\n",
               currTime.tv_sec,       currTime.tv_usec,
               state->timeout.tv_sec, state->timeout.tv_usec);
        
        return 1;
    }
    else if((currTime.tv_sec == state->timeout.tv_sec) && (currTime.tv_usec >= state->timeout.tv_usec)) {
        printf("[2] timeout; currTime = %li.%06i - timeout = %li.%06i\n",
               currTime.tv_sec,       currTime.tv_usec,
               state->timeout.tv_sec, state->timeout.tv_usec);
        
        return 1;
    }
    //  else {
    //      printf("[3] no timeout; currTime = %li.%06i - timeout = %li.%06i\n",
    //          currTime.tv_sec,       currTime.tv_usec,
    //          state->timeout.tv_sec, state->timeout.tv_usec);
    //      return 0;
    //  }
    
    return 0;
}


//=====================================================================================================================
//
// function()
//
// Function...
//
//
// Assumes:
//  -
//
//
// Parameters:
//  -
//
//
// Returns:
//  -
//
//
// Notes:
//
//
//=====================================================================================================================

int getEcolinkPacket(EcolinkParserState *state, unsigned char *pkt, unsigned char *pktLen) {
    
    if(*pktLen < state->bufLen) {
        return ECOLINK_ERR_BUFFER_TOO_SMALL;
    }
    
    memcpy(pkt, state->buf, state->bufLen);
    *pktLen = state->bufLen;
    
    resetEcolinkParser(state);
    
    
    return ECOLINK_OK;
}


//=====================================================================================================================
//
// function()
//
// Function...
//
//
// Assumes:
//  -
//
//
// Parameters:
//  -
//
//
// Returns:
//  -
//
//
// Notes:
//
//
//=====================================================================================================================

void printEcolinkParserState(EcolinkParserState *state) {
    
    int     i;
    
    
    switch(state->state) {
        case READY:            { printf("state:        [%i] READY\n",            READY           ); break; }
        case START_OF_PACKET:  { printf("state:        [%i] START_OF_PACKET\n",  START_OF_PACKET ); break; }
        case READ_DATA:        { printf("state:        [%i] READ_DATA\n",        READ_DATA       ); break; }
        case PACKET_AVAILABLE: { printf("state:        [%i] PACKET_AVAILABLE\n", PACKET_AVAILABLE); break; }
        default:               { printf("state:            UNKOWN_STATE\n"                       ); break; }
    }
    printf("timeout:      %li.%06i\n", state->timeout.tv_sec,      state->timeout.tv_usec);
    printf("timeoutValue: %li.%06i\n", state->timeoutValue.tv_sec, state->timeoutValue.tv_usec);
    printf("bufLen:       %i\n", state->bufLen);
    printf("buf:          [00]");
    
    
    for(i=0; i<state->bufLen; i++) {
        
        if(((i % 16) == 0) && (i != 0)) { 
            printf("\n           [%02i]", (i / 16)); 
        }
        else if(((i %  8) == 0) && (i != 0)) {
            printf(" -");
        }
        
        printf(" %02X", state->buf[i]);
    }
    printf("\n");
    
    
    
    printf("\n");
}
